---
title: industrial
description: “Every industrial revolution brings along a learning revolution.” — Alexander DE CROO
weight: 5
featured_image: 20230726-003.jpg
sort_by: Name
sort_order: desc
menus: main
---