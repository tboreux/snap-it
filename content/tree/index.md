---
title: tree
description: “Nothing on this earth is standing still. It's either growing or it's dying. No matter if it's a tree or a human being.” — Lou HOLTZ
weight: 8
featured_image: 20211120-001.jpg
sort_by: Name
sort_order: desc
menus: main
---