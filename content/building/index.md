---
title: building
description: “You can't build a great building on a weak foundation. You must have a solid foundation if you're going to have a strong superstructure.” — Gordon B. HINCKLEY
weight: 2
featured_image: 20220307-001.jpg
sort_by: Name
sort_order: desc
menus: main
---