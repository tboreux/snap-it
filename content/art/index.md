---
title: art
description: “Life beats down and crushes the soul and art reminds you that you have one.” — Stella ADLER
weight: 1
featured_image: 20230815-021.jpg
sort_by: Name
sort_order: desc
menus: main
---