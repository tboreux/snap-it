---
title: war
description: “War does not determine who is right - only who is left.” — Bertrand RUSSELL
weight: 9
featured_image: 20230815-005.jpg
sort_by: Name
sort_order: desc
menus: main
---