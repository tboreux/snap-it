---
title: nature
description: “In the depth of winter I finally learned that there was in me an invincible summer.” — Albert CAMUS
weight: 6
featured_image: 20211120-002.jpg
sort_by: Name
sort_order: desc
menus: main
---