# snap-it 📷

This website is my photo gallery. The pictures are reparted in album regarding their theme.

## Demo

This website is accessible from [https://snap-it.boreux.work](https://snap-it.boreux.work), feel free to visit and test it! 

But you'll find in the next section some screenshots.


## Screenshots

### Home

![App Screenshot](screenshots/01.home.png)

### Album

![App Screenshot](screenshots/02.album.png)


## Tech Stack

![Static Badge](https://img.shields.io/badge/Hugo-0.127.0-4CAF50?style=flat-square&logo=hugo&logoColor=white)

## License

[![MIT License](https://img.shields.io/badge/License-MIT-4CAF50?style=flat-square)](https://choosealicense.com/licenses/mit/)

